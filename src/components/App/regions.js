const regions = [
    [{
        name: 'A',
        col: 1,
        row: 1,
        title: "Settings",
        level: 0,
    },
    {
        name: 'B',
        col: 2,
        row: 1,
        title: "Graphics",
        level: 1,
    },
    {
        name: 'C',
        col: 3,
        row: 1,
        title: "Actionables",
        level: 2,
    }],
    [{
        name: 'D',
        col: 1,
        row: 2,
        title: "Mood",
        level: 1,
    },
    {
        name: 'E',
        col: 2,
        row: 2,
        title: "Travel",
        level: 2,
    },
    {
        name: 'F',
        col: 3,
        row: 2,
        title: "Calendar",
        level: 3,
    }],
    [{
        name: 'G',
        col: 1,
        row: 3,
        title: "Hoidays",
        level: 2,
    },
    {
        name: 'H',
        col: 2,
        row: 3,
        title: "Weather",
        level: 3,
    },
    {
        name: 'I',
        col: 3,
        row: 3,
        title: "",
        level: 4,
    }]
];

export default regions;